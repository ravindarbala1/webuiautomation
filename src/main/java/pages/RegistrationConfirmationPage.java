package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationConfirmationPage {
    private WebDriver driver;
    private By confirmationMsg = By.id("SandboxMessage");

    public RegistrationConfirmationPage(WebDriver driver){
        this.driver=driver;
    }


    public String confirmationMessage(){
        return driver.findElement(confirmationMsg).getText();
    }
}
