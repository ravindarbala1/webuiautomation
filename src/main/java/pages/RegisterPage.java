package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class RegisterPage {

    private WebDriver driver;


    private By email = By.name("LoginDetails$EmailAddressTextBox");
    private By pwd = By.name("LoginDetails$PasswordTextBox");
    private By pwd_confirm = By.name("LoginDetails$ConfirmPasswordTextBox");
    private By userName = By.name("LoginDetails$UserNameTextBox");
    private By firstName = By.id("ContactDetails_FirstNameTextBox");
    private By lastName = By.id("ContactDetails_LastNameTextBox");
    private By genderMale = By.xpath("//label[text()=' Man']");
    private By dobDay = By.xpath("//select[@id='ContactDetails_DobDay']");
    private By dobMonth = By.xpath("//select[@id='ContactDetails_DobMonth']");
    private By dobYear = By.id("ContactDetails_DobYear");
    private By areaCode = By.id("ContactDetails_ContactPhoneAreaCodeDropDown");
    private By phone = By.id("ContactDetails_ContactPhoneTextBox");
    private By country = By.xpath("//input[value='New Zealand']");
    private By address = By.xpath("//input[@class='js-txt-search-address']");
    private By district = By.id("ContactDetails_ClosestSuburbDropDown");
    private By terms = By.cssSelector("input[name='TnCCheckbox']");
    private By submit_btn = By.name("SubmitButton");


    public RegisterPage(WebDriver driver) {
        this.driver = driver;

    }

    public void setEmail(String emailVal) {

        driver.findElement(email).click();
        driver.findElement(email).sendKeys(emailVal);
    }

    public void setPwd(String pass) {
        driver.findElement(pwd).clear();
        driver.findElement(pwd).sendKeys(pass);
    }

    public void setConfirmPwd(String confirmPass) {
        driver.findElement(pwd_confirm).clear();
        driver.findElement(pwd_confirm).sendKeys(confirmPass);
    }

    public void setUserName(String user) {
        driver.findElement(userName).clear();
        StringBuilder sb = new StringBuilder();
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        char randomChar = alphabet.charAt(random.nextInt(alphabet.length()));
        sb.append(user);
        sb.append(randomChar);
        driver.findElement(userName).sendKeys(sb.toString());
    }

    public void setFirstName(String first) {
        driver.findElement(firstName).clear();
        driver.findElement(firstName).sendKeys(first);

    }

    public void setLastName(String last) {
        driver.findElement(lastName).clear();
        driver.findElement(lastName).sendKeys(last);

    }

    public void setGender(String val) {
        driver.findElement(genderMale).click();


    }

    public void setDobDate(String val) {
        Select select = new Select(driver.findElement(dobDay));
        select.selectByVisibleText(val);

    }

    public void setDobMonth(String val) {
        Select select = new Select(driver.findElement(dobMonth));
        select.selectByVisibleText(val);
    }

    public void setDobYear(String val) {
        driver.findElement(dobYear).sendKeys(val);
    }

    public void setAreaCode(String val) {

        Select select = new Select(driver.findElement(areaCode));
        select.selectByVisibleText(val);
    }

    public void setPhone(String phoneNumber) {
        driver.findElement(phone).clear();
        driver.findElement(phone).sendKeys(phoneNumber);
    }

    public void setCountry(String val) {
        driver.findElement(country).click();
    }

    public void setAddress(String addr) {
        driver.findElement(address).click();
        driver.findElement(address).sendKeys(addr);
        List<WebElement> optionsToSelect =driver.findElements(By.cssSelector("div[class='auto-complete-list purchase-address-list address-finder-list'] span"));

        for (WebElement option : optionsToSelect) {
            System.out.println(option);
            if (option.getText().contains(addr)) {
                System.out.println("Trying to select: " + addr);
                option.click();
                driver.manage().timeouts().implicitlyWait(2000,TimeUnit.SECONDS);
                break;
            }
        }

    }


public void selectDistrict(String val){
        driver.findElement(district).click();
    List<WebElement> option = driver.findElement(district).findElements(By.tagName("option"));
    for(WebElement ele:option){
        if(ele.getText().contains(val)){
            ele.click();
            break;
        }

    }
}


    public void selectConditions() {
        driver.manage().timeouts().implicitlyWait(2000,TimeUnit.SECONDS);
        new WebDriverWait(driver, 100).until(ExpectedConditions.elementToBeClickable(terms)).click();

    }

    public void clickOnSubmit() {

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)", "");
        driver.findElement(submit_btn).click();

    }
}
