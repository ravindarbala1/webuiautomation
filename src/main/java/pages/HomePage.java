package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class HomePage {

    private WebDriver driver;

    private By register_lnk = By.xpath("//a[text()='Register'] ");

    private By browseLink = By.cssSelector("li[id='BrowseDropDown']");

    private By linksUnderBrowser = By.cssSelector("ul[class='nav header-links']  ul  li a");

    private By booksLink = By.xpath("//*[@id='main-box-categories']//following::a[text()='Books']");


    public HomePage(WebDriver driver){
        this.driver =driver;

    }

    public void registerClick(){
        driver.findElement(register_lnk).click();
        driver.manage().timeouts().implicitlyWait(5000,TimeUnit.SECONDS);


    }
    public void clickOnBrowse(){
        driver.findElement(browseLink).click();
    }

    public void clickOnLinksUnderBrowse() {
        List<WebElement> elements = driver.findElements(linksUnderBrowser);
        for (WebElement element : elements) {
            element.click();
            break;
        }
    }
        public List<WebElement> getListOfLinks(){
            return driver.findElements(linksUnderBrowser);
        }
        public void clickOnLink(){
        driver.findElement(booksLink).click();
        }

    }




