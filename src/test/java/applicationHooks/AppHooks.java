package applicationHooks;

import driverFactory.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.WebDriver;
import util.ConfigRead;

import java.util.Properties;

public class AppHooks {

    private DriverFactory driverFactory;
    private WebDriver driver;
    Properties prop;

    @Before(order = 0)
    public void getProperty() {
        ConfigRead configRead = new ConfigRead();
        prop = configRead.init_prop();

    }

    @Before(order = 1)
    public void launchBrowser() {
        String BrowserName = prop.getProperty("browser");
        driverFactory = new DriverFactory();
        driver = driverFactory.init_driver(BrowserName);
    }

    @After(order = 0)
    public void after(Scenario scenario) {
        driver.quit();
    }
}
