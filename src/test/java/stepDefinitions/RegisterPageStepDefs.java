package stepDefinitions;

import driverFactory.DriverFactory;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import pages.RegisterPage;

public class RegisterPageStepDefs {
    private RegisterPage registerPage = new RegisterPage(DriverFactory.getDriver());


    @Then("user enter email {string}")
    public void userEnterEmail(String email) {
        registerPage.setEmail(email);

    }

    @And("user enter password {string}")
    public void userEnterPassword(String pass) {
        registerPage.setPwd(pass);
    }

    @And("user enter confirm password {string}")
    public void userEnterConfirmPassword(String confirmpwd) {
        registerPage.setConfirmPwd(confirmpwd);
    }

    @And("user enter username {string}")
    public void userEnterUsername(String userName) {
        registerPage.setUserName(userName);

    }

    @And("user enter firstname {string}")
    public void userEnterFirstname(String firstName) {
        registerPage.setFirstName(firstName);

    }

    @And("user enter lastname {string}")
    public void userEnterLastname(String last) {
        registerPage.setLastName(last);
    }

    @And("user select gender {string}")
    public void userSelectGender(String gender) {
        registerPage.setGender(gender);
    }

    @And("user select day {string}")
    public void userSelectDay(String val) {
        registerPage.setDobDate(val);

    }

    @And("user select month {string}")
    public void userSelectMonth(String month) {
        registerPage.setDobMonth(month);

    }

    @And("user select year {string}")
    public void userSelectYear(String year) {
        registerPage.setDobYear(year);
    }

    @And("user select code {string}")
    public void userSelectCode(String code) {
        registerPage.setAreaCode(code);
    }

    @And("user enter phone {string}")
    public void userEnterPhone(String phone) {
        registerPage.setPhone(phone);
    }

    @And("user select country {string}")
    public void userSelectCountry(String name) {
        registerPage.setCountry(name);
    }

    @And("user enter address {string}")
    public void userEnterAddress(String address) {
        registerPage.setAddress(address);
    }


    @And("user select district {string}")
    public void userSelectDistrict(String dis) {
        registerPage.selectDistrict(dis);

    }

    @And("user accept conditions")
    public void userSelectConditions() {
        registerPage.selectConditions();

    }

    @Then("user click on create account")
    public void userClickOnCreateAccount() {
        registerPage.clickOnSubmit();
    }
}


