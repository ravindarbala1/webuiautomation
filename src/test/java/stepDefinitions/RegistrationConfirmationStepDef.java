package stepDefinitions;

import driverFactory.DriverFactory;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.RegistrationConfirmationPage;

public class RegistrationConfirmationStepDef {

RegistrationConfirmationPage confirmationPage = new RegistrationConfirmationPage(DriverFactory.getDriver());

    @Then("user sees success message")
    public void userSeesSuccessMessage() {
        Assert.assertTrue(confirmationPage.confirmationMessage().contains("Thank you for registering"));

    }

}
