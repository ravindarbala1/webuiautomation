package stepDefinitions;

import driverFactory.DriverFactory;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pages.HomePage;

public class HomePageStepDefs {

    private HomePage homePage = new HomePage(DriverFactory.getDriver());

    @Given("user is on home page")
    public void userIsOnHomePage() {

        DriverFactory.getDriver().get("https://www.tmsandbox.co.nz/");
    }

    @And("user click on register link")
    public void userClickOnRegisterLink() {
        homePage.registerClick();
    }

    @When("user click on Browse link")
    public void userClickOnBrowseLink() {
        homePage.clickOnBrowse();
    }

    @Then("user can see links under browse")
    public void userCanSeeLinksUnderBrowse() {
        Assert.assertTrue(homePage.getListOfLinks().size() > 0);
    }

    @And("user clicks on the links")
    public void userClicksOnTheLinks() {
        homePage.clickOnLinksUnderBrowse();
    }

    @When("user clicks on the {string} link")
    public void userClicksOnTheLink(String val) {
        homePage.clickOnLink();
    }
}
