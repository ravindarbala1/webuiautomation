Feature: Home page feature

  Scenario: Home page
    Given user is on home page
    And user click on register link

    Scenario: Verify whether user is able to navigate to the links under Browse
      Given user is on home page
      When user click on Browse link
      Then user can see links under browse
      And  user clicks on the links

    Scenario: verify whether user able to click the links on homepage
      Given user is on home page
      When  user clicks on the "Books" link
