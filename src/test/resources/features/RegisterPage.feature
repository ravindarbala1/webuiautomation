Feature: As a user i would like to signup so that i can login into the application

  Scenario Outline: verify whether user is able to register or not

    Given user is on home page
    And user click on register link
    Then user enter email "<email>"
    And user enter password "<pwd>"
    And user enter confirm password "<confirmPwd>"
    And user enter username "<userName>"
    And user enter firstname "<firstName>"
    And user enter lastname "<lastName>"
    And user select gender "<gender>"
    And user select day "<day>"
    And user select month "<month>"
    And user select year "<year>"
    And user select code "<areaCode>"
    And user enter phone "<phone>"
    And user enter address "<address>"
    And user accept conditions
    And user select district "<district>"
    Then user click on create account
    Then  user sees success message
    Examples:
      | email                | pwd        | confirmPwd | userName | firstName | lastName | gender | day | month   | year | areaCode | phone     | country     | address       | district |
      | ravindar20@gmail.com | testing123 | testing123 | userone123    | test1     | test2    | male   | 1   | January | 1978 | 022      | 6204654 | New Zealand | 207 Sunnynook | Auckland |